﻿using NUnit.Framework;
using Moq;
using BookService;

namespace BookService.Test
{
    [TestFixture]
    public class MockTests
    {
        [Test]
        public void Test()
        {
            var mock = new Mock<IBookListStorage>();
            mock.Setup(x => x.Books).Returns(new System.Collections.Generic.List<Book>());
            mock.Setup(x => x.Contains(It.IsAny<Book>()));

            var stub = mock.Object;

            Assert.AreEqual(0, stub.Books.Count);
        }
    }
}
