﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookService
{
    /// <summary>
    /// Class for validating currency strings.
    /// </summary>
    public static class IsoCurrencyValidator
    {
        /// <summary>
        /// Determines whether a specified string is a valid ISO currency symbol.
        /// </summary>
        /// <param name="currency">Currency string to check.</param>
        /// <returns>
        /// <see langword="true"/> if <paramref name="currency"/> is a valid ISO currency symbol; <see langword="false"/> otherwise.
        /// </returns>
        /// <exception cref="ArgumentException">Thrown if currency is null or empty or whitespace or white-space.</exception>
        public static bool IsValid(string currency)
        {
            if (string.IsNullOrEmpty(currency) || string.IsNullOrWhiteSpace(currency))
            {
                throw new ArgumentException("Currency is null or empty or whitespace", nameof(currency));
            }

            try
            {
                RegionInfo regionInfo = new RegionInfo(currency[0..2]);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
