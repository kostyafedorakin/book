﻿using System;
using System.Collections.Generic;


namespace BookService
{
    public class BookListService
    {
        private IBookListStorage _storage;
        private List<Book> _books = new();

        public BookListService(IBookListStorage storage)
        {
            _storage = storage ?? throw new ArgumentNullException(nameof(storage));
        }

        public void Add(Book book)
        {
            if (book is null)
            {
                throw new ArgumentNullException(nameof(book));
            }

            if (_books.Contains(book) || _storage.Books.Contains(book))
            {
                throw new ArgumentException("The book was added earlier.", nameof(book));
            }

            _books.Add(book);
        }

        public void Remove(Book book)
        {
            if (book is null)
            {
                throw new ArgumentNullException(nameof(book));
            }

            if (_books.Contains(book))
            {
                _books.Remove(book);
                return;
            }

            _storage.Remove(book);
        }

        public IEnumerable<Book> FindByTag(string author)
        {
            foreach (var book in _books)
            {
                if (string.Equals(book.Author, author, StringComparison.Ordinal)) 
                {
                    yield return book;
                }
            }
        }

        public IEnumerable<Book> FindByTag(string author, string title)
        {
            foreach (var book in this.FindByTag(author))
            {
                if (string.Equals(book.Title, title, StringComparison.Ordinal))
                {
                    yield return book;
                }
            }
        }

        public IEnumerable<Book> FindByTag(string author, string title, string publisher)
        {
            foreach (var book in this.FindByTag(author, title))
            {
                if (string.Equals(book.Publisher, publisher, StringComparison.Ordinal))
                {
                    yield return book;
                }
            }
        }

        public IEnumerable<Book> FindByTag(string author, string title, string publisher, int pages)
        {
            foreach (var book in this.FindByTag(author, title, publisher))
            {
                if (book.Pages == pages)
                {
                    yield return book;
                }
            }
        }

        public IEnumerable<Book> FindByTag(string author, string title, string publisher, int pages, string isbn)
        {
            foreach (var book in this.FindByTag(author, title, publisher, pages))
            {
                if (string.Equals(book.ISBN, isbn, StringComparison.Ordinal))
                {
                    yield return book;
                }
            }
        }

        public IEnumerable<Book> FindByTag(string author, string title, string publisher, int pages, string isbn, decimal price)
        {
            foreach (var book in this.FindByTag(author, title, publisher, pages, isbn))
            {
                if (book.Price == price)
                {
                    yield return book;
                }
            }
        }

        public IEnumerable<Book> FindByTag(string author, string title, string publisher, int pages, string isbn, decimal price, string currency)
        {
            foreach (var book in this.FindByTag(author, title, publisher, pages, isbn))
            {
                if (string.Equals(book.Currency, currency, StringComparison.Ordinal))
                {
                    yield return book;
                }
            }
        }

        public IEnumerable<Book> Load()
        {
            foreach (var book in _books)
            {
                yield return book;
            }

            foreach (var book in _storage.Books)
            {
                yield return book;
            }
        }

        public void Save()
        {
            foreach (var book in _books)
            {
                if (!_storage.Contains(book))
                {
                    _storage.Add(book);
                }
            }
        }
    }
}
