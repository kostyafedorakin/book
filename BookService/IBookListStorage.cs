﻿using System.Collections.Generic;

namespace BookService
{
    public interface IBookListStorage
    {
        List<Book> Books { get; }

        void Add(Book book);
        bool Contains(Book book);
        void Remove(Book book);
    }
}