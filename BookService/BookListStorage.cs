﻿using System;
using System.Collections.Generic;
using System.Data;

namespace BookService
{
    public class BookListStorage : IBookListStorage
    {
        private List<Book> _books = new();

        public List<Book> Books
        {
            get => _books;
        }

        public void Add(Book book)
        {
            if (book is null)
            {
                throw new ArgumentNullException(nameof(book));
            }

            if (_books.Contains(book))
            {
                throw new ArgumentException("The book was added earlier.", nameof(book));
            }

            _books.Add(book);
        }

        public void Remove(Book book)
        {
            if (book is null)
            {
                throw new ArgumentNullException(nameof(book));
            }

            if (_books.Contains(book))
            {
                _books.Remove(book);
            }

            throw new ArgumentException("The book was not found.", nameof(book));
        }

        public bool Contains(Book book) => _books.Contains(book);
    }
}
