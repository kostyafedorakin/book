﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;

namespace BookService
{
    /// <summary>
    /// Represents the book as a type of publication.
    /// </summary>
    public sealed class Book : IEquatable<Book>, IComparable<Book>, IComparable
    {
        private bool published;
        private DateTime datePublished;
        private int totalPages;

        /// <summary>
        /// Initializes a new instance of the <see cref="Book"/> class.
        /// </summary>
        /// <param name="author">Autor of the book.</param>
        /// <param name="title">Title of the book.</param>
        /// <param name="publisher">Publisher of the book.</param>
        /// <exception cref="ArgumentNullException">Throw when author or title or publisher is null.</exception>
        public Book(string author, string title, string publisher)
            : this(author, title, publisher, "000")
        {
            this.Author = author;
            this.Title = title;
            this.Publisher = publisher;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Book"/> class.
        /// </summary>
        /// <param name="author">Autor of the book.</param>
        /// <param name="title">Title of the book.</param>
        /// <param name="publisher">Publisher of the book.</param>
        /// <param name="isbn">International Standard Book Number.</param>
        /// <exception cref="ArgumentNullException">Throw when author or title or publisher or ISBN is null.</exception>
        public Book(string author, string title, string publisher, string isbn)
        {
            if (author is null)
            {
                throw new ArgumentNullException(nameof(author));
            }

            if (title is null)
            {
                throw new ArgumentNullException(nameof(title));
            }

            if (publisher is null)
            {
                throw new ArgumentNullException(nameof(publisher));
            }

            if (isbn is null)
            {
                throw new ArgumentNullException(nameof(isbn));
            }

            this.Author = author;
            this.Title = title;
            this.Publisher = publisher;
            this.ISBN = isbn;
        }

        /// <summary>
        /// Gets author of the book.
        /// </summary>
        public string Author { get; }

        // Add code here

        /// <summary>
        /// Gets title of the book.
        /// </summary>
        public string Title { get; }

        // Add code here

        /// <summary>
        /// Gets publisher of the book.
        /// </summary>
        public string Publisher { get; }

        // Add code here

        /// <summary>
        /// Gets or sets total pages in the book.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">Throw when Pages less or equal zero.</exception>
        public int Pages
        {
            get => this.totalPages;
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(value));
                }

                this.totalPages = value;
            }
        }

        // Add code here

        /// <summary>
        /// Gets International Standard Book Number.
        /// </summary>
        public string ISBN { get; }

        // Add code here

        /// <summary>
        /// Gets price.
        /// </summary>
        public decimal Price { get; private set; }

        // Add code here

        /// <summary>
        /// Gets currency.
        /// </summary>
        public string Currency { get; private set; }

        // Add code here

        /// <summary>
        /// Publishes the book if it has not yet been published.
        /// </summary>
        /// <param name="dateTime">Date of publish.</param>
        public void Publish(DateTime dateTime)
        {
            if (!this.published)
            {
                this.published = true;
                this.datePublished = dateTime;
            }
        }

        /// <summary>
        /// String representation of book.
        /// </summary>
        /// <returns>Representation of book.</returns>
        public override string ToString() => $"{this.Title} by {this.Author}";

        /// <summary>
        /// Gets a information about time of publish.
        /// </summary>
        /// <returns>The string "NYP" if book not published, and the value of the datePublished if it is published.</returns>
        public string GetPublicationDate()
        {
            if (this.published)
            {
                return this.datePublished.ToString("MM/dd/yyyy", CultureInfo.CurrentCulture).Replace('.', '/');
            }

            return "NYP";
        }

        /// <summary>
        /// Sets the prise and currency of the book.
        /// </summary>
        /// <param name="price">Price of book.</param>
        /// <param name="currency">Currency of book.</param>
        /// <exception cref="ArgumentException">Throw when Price less than zero or currency is invalid.</exception>
        /// <exception cref="ArgumentNullException">Throw when currency is null.</exception>
        public void SetPrice(decimal price, string currency)
        {
            if (currency is null)
            {
                throw new ArgumentNullException(nameof(currency));
            }

            if (!IsoCurrencyValidator.IsValid(currency[0..2]))
            {
                throw new ArgumentException("Currency is invalid.");
            }

            if (price < 0)
            {
                throw new ArgumentException("Price is less then zero.", nameof(price));
            }

            this.Price = price;
            this.Currency = currency;
        }

        public override int GetHashCode() => this.Pages;

        public int CompareTo([AllowNull] Book other)
        {
            if (other is null)
            {
                throw new ArgumentNullException(nameof(other));
            }

            return this.Title.Length - other.Title.Length;
        }

        public bool Equals([AllowNull] Book other)
        {
            if (other is null)
            {
                throw new ArgumentNullException(nameof(other));
            }

            return this.ISBN == other.ISBN;
        }

        public int CompareTo(object obj)
        {
            if (obj is null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            try
            {
                return this.CompareTo(obj as Book);
            }
            catch
            {
                throw new ArgumentException("It`s impossible to cast an object into Book object.", nameof(obj));
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            try
            {
                return this.Equals(obj as Book);
            }
            catch
            {
                throw new ArgumentException("It`s impossible to cast an object into Book object.", nameof(obj));
            }
        }
    }
}
